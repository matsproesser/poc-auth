# Lib usage quickstart
This readme  contains basic instructions to use the dependency injection
framework and vert.x cover lib for B2B and some explanation about components.

## Components
### Cover (cover)
This package holds classes responsible for simplifying service(Verticle) implementation and
binding vert.x verticle with the dependency injection framework.
- **_`B2BLauncher`_**: Has a single static method `launch(B2Bverticle|Supplier<B2BVerticle>)` 
responsible inject dependencies into service and start application.
- **_`B2BAbstractVerticle`_**: Abstract class to cover the basic _`AbstractVerticle`_ with dependency injection and
common features used into B2B services.
 
### Dependency injection (di)
This package contains all engine responsible for dependency injection with dynamic property update.
The components of public API are:
- **_`B2BDependencyManager`_**: Orchestrate dependencies with the dynamic configuration. Implements _`DependencyConfigure`_ and _`DependencyResolver`_ used into cover.
- **_`DependencyConfigure`_**: Interface to configure and bind dependencies into _`B2BDependencyManager`_
- **_`DependencyResolver`_**: Interface to request dependency observables from _`B2BDependencyManager`_
- **_`DependencyBindConfiguration`_**: Interface to map a dependency to be configured on _`DependencyConfigure`_
- **_`DependencyBindBuilder`_** and **_`DependencyBindWrapper`_**: Helpers to configure dependencies. Implements/builds _`DependencyBindConfiguration`_

### ReactiveX helpers and extensions (reactive)
This package Contains some Observer/Single implementations useful into the handler oriented implementations.
- **_`ObservableHandler`_**: Used to convert a method with handler of multiple results into a ObservableSource 
- **_`SingleHandler`_**: Used to convert a method with handler of single result into a SingleSource
- **_`SingleHelpers`_** and **_`ObservableHelpers`_**: Has a method o use with flatMap and unwrap AsyncResults.

## Examples
### Basic verticle
To create your first vert.x service (verticle) using the B2B cover lib you need two components

 1 - The **_`B2bLauncher`_** responsible to orchestrate all dependencies and bindings of dependency injection
framework and B2B cover.
```
    public static void main(String[] args) {
        B2BLauncher.launch(MyVerticle::new);
    }
```

 2 - The **_`B2BAbstractVerticle`_** to implement our service behavior using dependency injection feature and bind with vert.x framework. 
This abstract class has some methods to be implemented:
- _**`start(DependencyResolver dependencyResolver)`**_ (**required**): All the application startup logic must be here like service instantiation and routes configuration.

- _**`configure(DependencyConfigure configurer)`**_ (_optional_): Override this method to use the DependencyConfigure instance and configure custom dependencies.

- _**`configChanged(ConfigChange change)`**_(_optional_): This method is called by ConfigRetriever when any configSource has changes (used on B2BLauncher). This method is useful only in cases where the
past state matters and need to be compared otherwise you should use the `config()` method to always get an updated config tree.

- _**`configRoutes(Handler<Router> routerHandler)`**_(_**template**_): This method is not overridable, but a template to be used to expose endpoints.

```
public class MyVerticle extends B2BAbstractVerticle {

    @Override
    public void configure(DependencyConfigure configure) {
        DependencyBindBuilder.fromType(UserRepository.class)
                        .withCreator((configs, resolver)-> new UserRepository(resolver.get(MongoClient.class)))
    }

    @Override
    public void start(DependencyResolver dR) {
        UserService userService = new AuthService(dR.get(UserRepository.class));
        configRoutes(router -> {
            router.route(HttpMethod.POST, "/create").handler(userService::create);
            router.route(HttpMethod.POST, "/list").handler(userService::list);
            router.route(HttpMethod.POST, "/details").handler(userService::getId);
        });
    }
}
```
Putting all together you will have a verticle started with dynamic config and three endpoints available. 

