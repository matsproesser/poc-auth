package poc.graphql.common;

import graphql.schema.DataFetchingEnvironment;
import io.vertx.core.json.JsonObject;

@FunctionalInterface
public interface QueryFilter {
    JsonObject getFilter(DataFetchingEnvironment environment);
}
