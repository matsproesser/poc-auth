package poc.graphql.common;

import io.reactivex.rxjava3.core.Maybe;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.Single;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.mongo.MongoClient;
import io.vertx.ext.mongo.MongoClientDeleteResult;
import poc.lib.reactive.SingleAsyncHandler;
import poc.lib.reactive.SingleHelpers;

import java.util.List;

import static java.util.stream.Collectors.toList;
import static poc.lib.reactive.SingleHelpers.asyncNullableUnwrap;

public abstract class AbstractRepository<T> {
    private final Class<T> clazz;
    private MongoClient mongoClient;
    private final String collectionName;

    public AbstractRepository(Observable<MongoClient> mongoClientSource, Class<T> clazz, String collection) {
        this.clazz = clazz;
        this.collectionName = collection;
        mongoClientSource.subscribe(mongoClient -> this.mongoClient = mongoClient);
    }

    public Maybe<String> upsert(T entity) {
        return new SingleAsyncHandler<String>()
                .handling(handler -> {
                    JsonObject document = JsonObject.mapFrom(entity);
                    if (document.getString("_id") == null) document.remove("_id");
                    mongoClient.save(collectionName, document, handler);
                })
                .flatMap(asyncNullableUnwrap()).mapOptional(o -> o);
    }

    public Single<T> updateById(String id, JsonObject update) {
        return new SingleAsyncHandler<JsonObject>().handling(handler -> mongoClient.findOneAndUpdate(collectionName, new JsonObject().put("_id", id), update, handler))
                .flatMap(SingleHelpers.asyncUnwrap()).map(jo -> jo.mapTo(clazz));
    }

    public Single<List<T>> findAll() {
        return new SingleAsyncHandler<List<JsonObject>>()
                .handling(handler -> mongoClient.find(collectionName, new JsonObject(), handler))
                .flatMap(SingleHelpers.asyncUnwrap())
                .map(list -> list.stream().map(jo -> jo.mapTo(clazz)).collect(toList()));
    }

    public Maybe<T> findById(String id) {
        JsonObject query = new JsonObject().put("_id", id);
        return new SingleAsyncHandler<List<JsonObject>>()
                .handling(handler -> mongoClient.find(collectionName, query, handler))
                .flatMap(SingleHelpers.asyncUnwrap())
                .mapOptional(list -> list.stream().map(jo -> jo.mapTo(clazz)).findFirst());
    }

    public Single<List<T>> findByIds(List<String> ids) {
        return new SingleAsyncHandler<List<JsonObject>>()
                .handling(handler -> {
                    JsonObject query = new JsonObject().put("_id", new JsonObject().put("$in", ids));
                    mongoClient.find(collectionName, query, handler);
                })
                .flatMap(SingleHelpers.asyncUnwrap())
                .map(list -> list.stream().map(jo -> jo.mapTo(clazz)).collect(toList()));
    }

    public Single<List<T>> findByQuery(JsonObject query) {
        return new SingleAsyncHandler<List<JsonObject>>()
                .handling(handler -> mongoClient.find(collectionName, query, handler))
                .flatMap(SingleHelpers.asyncUnwrap())
                .map(list -> list.stream().map(jo -> jo.mapTo(clazz)).collect(toList()));
    }

    public Single<Long> remove(String id) {
        return new SingleAsyncHandler<MongoClientDeleteResult>()
                .handling(handler -> mongoClient.removeDocument(collectionName, new JsonObject().put("_id", id), handler))
                .flatMap(SingleHelpers.asyncUnwrap())
                .map(result -> result.getRemovedCount());
    }
}
