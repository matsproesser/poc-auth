package poc.graphql.common;

import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import io.reactivex.rxjava3.core.Observable;
import io.vertx.core.json.JsonObject;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletionStage;

public class BasicQueries<T> {

    private AbstractRepository<T> service;

    public BasicQueries(Observable<? extends AbstractRepository<T>> serviceObservable) {
        serviceObservable.subscribe(service -> this.service = service);
    }

    public CompletionStage<List<T>> queryRoot(DataFetchingEnvironment env) {
        return service.findByQuery(new JsonObject()).toCompletionStage();
    }

    public CompletionStage<T> querySubId(DataFetchingEnvironment env) {
        Object source = env.getSource();
        String id = null;
        try {
            Field targetField = source.getClass().getField(env.getField().getName());
            targetField.setAccessible(true);
            id = targetField.get(source).toString();
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return service.findById(id).toCompletionStage();
    }

    public DataFetcher<CompletionStage<List<T>>> queryListWithFilter(QueryFilter filter) {
        return environment -> service.findByQuery(filter.getFilter(environment)).toCompletionStage();
    }

    public DataFetcher<CompletionStage<T>> queryOneWithFilter(QueryFilter filter) {
        return environment -> service
                .findByQuery(filter.getFilter(environment))
                .mapOptional(list -> Optional.ofNullable(list.get(0)))
                .toCompletionStage();
    }
}
