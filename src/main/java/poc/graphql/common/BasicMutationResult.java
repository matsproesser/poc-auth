package poc.graphql.common;

import java.util.ArrayList;
import java.util.List;

public class BasicMutationResult {

    private final String id;
    private final Integer status;
    private final List<String> errors = new ArrayList<>();

    public BasicMutationResult(String id, Integer status, List<String> errors) {
        this.id = id;
        this.status = status;
        this.errors.addAll(errors);
    }
    public BasicMutationResult(String id, Integer status) {
        this.id = id;
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public Integer getStatus() {
        return status;
    }

    public List<String> getErrors() {
        return errors;
    }
}
