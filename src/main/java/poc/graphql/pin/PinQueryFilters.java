package poc.graphql.pin;

import graphql.schema.DataFetchingEnvironment;
import io.vertx.core.json.JsonObject;

public class PinQueryFilters {

    public static JsonObject owners(DataFetchingEnvironment environment) {
        Pin source = environment.getSource();
        return new JsonObject().put("pins", source.getId());
    }
}
