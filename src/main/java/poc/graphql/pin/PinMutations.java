package poc.graphql.pin;

import graphql.schema.DataFetchingEnvironment;
import io.reactivex.rxjava3.core.Observable;
import io.vertx.core.json.JsonObject;
import poc.graphql.common.BasicMutationResult;

import java.util.ArrayList;
import java.util.concurrent.CompletionStage;

public class PinMutations {

    private PinRepository repository;

    public PinMutations(Observable<? extends PinRepository> obs) {
        obs.subscribe(repository -> this.repository = repository);
    }

    public CompletionStage<BasicMutationResult> create(DataFetchingEnvironment environment) {
        Pin newPin = new Pin(environment.getArgument("name"), environment.getArgument("description"));
        return repository.upsert(newPin).map(result -> new BasicMutationResult(result, 200, new ArrayList<>())).toCompletionStage();
    }


    public CompletionStage<BasicMutationResult> archive(DataFetchingEnvironment environment) {
        String id = environment.getArgument("id");
        return repository.updateById(id, new JsonObject().put("archived", true))
                .map(pin -> new BasicMutationResult(pin.getId(), 200))
                .toCompletionStage();
    }
}
