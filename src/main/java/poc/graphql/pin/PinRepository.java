package poc.graphql.pin;

import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.Single;
import io.vertx.ext.mongo.MongoClient;
import poc.graphql.common.AbstractRepository;

public class PinRepository extends AbstractRepository<Pin> implements PinService {
    private static final String PIN_COLLECTION = "PinCollection";

    public PinRepository(Observable<MongoClient> mongoClientSource) {
        super(mongoClientSource, Pin.class, PIN_COLLECTION);
    }

    @Override
    public Single<Boolean> idExists(String id) {
        return findById(id).map(item -> true).defaultIfEmpty(false);
    }
}
