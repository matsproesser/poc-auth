package poc.graphql.pin;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.bson.codecs.pojo.annotations.BsonId;

public class Pin {

    @JsonProperty("_id")
    private String id;

    private String name;

    private String description;

    private boolean archived = false;

    private Pin() {

    }

    public Pin(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public boolean isArchived() {
        return archived;
    }
    @Override
    public String toString() {
        return "Pin{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
