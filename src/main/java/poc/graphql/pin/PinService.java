package poc.graphql.pin;

import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.core.Single;

public interface PinService {
    @NonNull Single<Boolean> idExists(String id);
}
