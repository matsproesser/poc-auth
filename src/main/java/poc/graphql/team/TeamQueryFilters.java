package poc.graphql.team;

import graphql.schema.DataFetchingEnvironment;
import io.vertx.core.json.JsonObject;
import poc.graphql.common.QueryFilter;

public class TeamQueryFilters {
    public static JsonObject members(DataFetchingEnvironment environment) {
        Team source = environment.getSource();
        return new JsonObject().put("teamId", source.getId());
    }
}
