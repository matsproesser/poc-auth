package poc.graphql.team;

import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.core.Single;

public interface TeamService {
    @NonNull Single<Boolean> idExists(String id);
}
