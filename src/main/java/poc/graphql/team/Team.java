package poc.graphql.team;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Team {

    @JsonProperty("_id")
    private String id;

    private String name;

    private String mainChannel;

    private Team() {

    }

    public Team(String name, String mainChannel) {
        this.name = name;
        this.mainChannel = mainChannel;
    }

    public String getName() {
        return name;
    }

    public String getMainChannel() {
        return mainChannel;
    }

    public String getId() {
        return id;
    }

    @Override
    public String toString() {
        return "Team{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", mainChannel='" + mainChannel + '\'' +
                '}';
    }
}
