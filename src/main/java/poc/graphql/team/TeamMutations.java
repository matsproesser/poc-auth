package poc.graphql.team;

import graphql.schema.DataFetchingEnvironment;
import io.reactivex.rxjava3.core.Observable;
import poc.graphql.common.BasicMutationResult;

import java.util.ArrayList;
import java.util.concurrent.CompletionStage;

public class TeamMutations {

    private TeamRepository repository;

    public TeamMutations(Observable<TeamRepository> observable) {
        observable.subscribe(teamRepository -> this.repository = teamRepository);
    }

    public CompletionStage<BasicMutationResult> create(DataFetchingEnvironment environment) {
        environment.getArgument("members"); //TODO: adicionar os membros no time
        Team team = new Team(environment.getArgument("name"), environment.getArgument("mainChannel"));
        return repository.upsert(team).map(result -> new BasicMutationResult(result, 200, new ArrayList<>())).toCompletionStage();
    }

    public CompletionStage<BasicMutationResult> remove(DataFetchingEnvironment environment) {
        String id = environment.getArgument("id");
        return repository.remove(id).map(count -> new BasicMutationResult(id, 200)).toCompletionStage();
    }
}
