package poc.graphql.team;

import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.Single;
import io.vertx.ext.mongo.MongoClient;
import poc.graphql.common.AbstractRepository;

public class TeamRepository extends AbstractRepository<Team> implements TeamService{
    private static final String TEAM_COLLECTION = "TeamsCollection";

    public TeamRepository(Observable<MongoClient> mongoClientSource) {
        super(mongoClientSource, Team.class, TEAM_COLLECTION);
    }

    @Override
    public Single<Boolean> idExists(String id) {
        return findById(id).map(item -> true).defaultIfEmpty(false);
    }
}
