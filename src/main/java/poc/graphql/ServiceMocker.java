package poc.graphql;

import io.reactivex.rxjava3.core.Maybe;
import io.reactivex.rxjava3.core.Single;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.ext.mongo.MongoClient;
import poc.graphql.pin.Pin;
import poc.graphql.pin.PinRepository;
import poc.graphql.team.Team;
import poc.graphql.team.TeamRepository;
import poc.graphql.user.User;
import poc.graphql.user.UserRepository;

import java.util.concurrent.atomic.AtomicInteger;

public class ServiceMocker {

    public static Boolean fillMock(MongoClient mongoClient, UserRepository userService, PinRepository pinService, TeamRepository teamService) {
        AtomicInteger dropCount = new AtomicInteger(0);
        Handler<AsyncResult<Void>> dropHandler = h -> {
            int i = dropCount.incrementAndGet();
            if (i == 3) {
                Single<String> rxGod = pinService.upsert(new Pin("RxGod", "The symbol of major reactive programming power")).toSingle();
                Single<String> limited = pinService.upsert(new Pin("Limited", "Destined to the one with limited skills")).toSingle();
                Single<String> randomPin = pinService.upsert(new Pin("RandomPin", "Just a pin with no value")).toSingle();

                teamService.upsert(new Team("Pricing", "pricing-private"));
                teamService.upsert(new Team("Migration", "migration-pod-1"));

                teamService.upsert(new Team("Kubernetes", "k8s-membersonly")).subscribe(teamId -> {
                    Single.zip(rxGod, randomPin, (pin1Id, pin2Id) -> {
                        User user = new User("Matheus Sproesser", "matheus.sproesser-ext@ab-inbev.com", teamId);
                        user.getPins().add(pin1Id);
                        user.getPins().add(pin2Id);
                        return userService.upsert(user);
                    }).subscribe();
                    userService.upsert(new User("Mateus Tanaka", "Mateus.Tanaka-ext@ab-Inbev.com", teamId));
                    userService.upsert(new User("Claudio Moretti", "Claudio.moretti-ext@ab-inbev.com", teamId));
                });

                teamService.upsert(new Team("Platform-services", "platform-services-members")).subscribe(
                        teamId -> userService.upsert(new User("Jaspion", "Fernando.jaspion-ext@ab-inbev.com", teamId))
                );

                System.out.println("Fills Completed");
            }
        };

        mongoClient.dropCollection("UserCollection", dropHandler);
        mongoClient.dropCollection("TeamsCollection", dropHandler);
        mongoClient.dropCollection("PinCollection", dropHandler);
        return true;
    }
}
