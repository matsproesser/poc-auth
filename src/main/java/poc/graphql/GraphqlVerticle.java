package poc.graphql;

import graphql.GraphQL;
import graphql.schema.GraphQLSchema;
import graphql.schema.idl.RuntimeWiring;
import graphql.schema.idl.SchemaGenerator;
import graphql.schema.idl.SchemaParser;
import graphql.schema.idl.TypeDefinitionRegistry;
import io.reactivex.rxjava3.core.Observable;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.mongo.MongoClient;
import io.vertx.ext.web.handler.graphql.GraphQLHandler;
import io.vertx.ext.web.handler.graphql.GraphiQLHandler;
import io.vertx.ext.web.handler.graphql.GraphiQLHandlerOptions;
import poc.graphql.common.BasicQueries;
import poc.graphql.pin.Pin;
import poc.graphql.pin.PinMutations;
import poc.graphql.pin.PinQueryFilters;
import poc.graphql.pin.PinRepository;
import poc.graphql.team.Team;
import poc.graphql.team.TeamMutations;
import poc.graphql.team.TeamQueryFilters;
import poc.graphql.team.TeamRepository;
import poc.graphql.user.User;
import poc.graphql.user.UserMutations;
import poc.graphql.user.UserQueryFilters;
import poc.graphql.user.UserRepository;
import poc.lib.cover.B2BAbstractVerticle;
import poc.lib.cover.B2BLauncher;
import poc.lib.di.DependencyBindBuilder;
import poc.lib.di.DependencyConfigure;
import poc.lib.di.DependencyResolver;

import java.util.function.BiFunction;

import static poc.lib.di.DependencyBindBuilder.fromType;

public class GraphqlVerticle extends B2BAbstractVerticle {
    public static void main(String[] args) {
        B2BLauncher.launch(GraphqlVerticle::new);
    }

    @Override
    public void configure(DependencyConfigure configurer) {
        fromType(UserRepository.class)
                .withCreator((configs, resolver)-> new UserRepository(resolver.get(MongoClient.class)))
                .neverUpdate().buildOn(configurer);
        fromType(TeamRepository.class)
                .withCreator((configs, resolver)-> new TeamRepository(resolver.get(MongoClient.class)))
                .neverUpdate().buildOn(configurer);
        fromType(PinRepository.class)
                .withCreator((configs, resolver)-> new PinRepository(resolver.get(MongoClient.class)))
                .neverUpdate().buildOn(configurer);
    }

    @Override
    public void start(DependencyResolver resolver) {
        Observable<MongoClient> mongoClientObservable = resolver.get(MongoClient.class);
        Observable<PinRepository> pinRepositoryObservable = resolver.get(PinRepository.class);
        Observable<TeamRepository> teamRepositoryObservable = resolver.get(TeamRepository.class);
        Observable<UserRepository> userRepositoryObservable = resolver.get(UserRepository.class);

        Observable.combineLatest(mongoClientObservable, userRepositoryObservable, pinRepositoryObservable, teamRepositoryObservable, ServiceMocker::fillMock).subscribe();

        BasicQueries<User> userQueries = new BasicQueries<>(userRepositoryObservable);
        BasicQueries<Team> teamQueries = new BasicQueries<>(teamRepositoryObservable);
        BasicQueries<Pin> pinQueries = new BasicQueries<>(pinRepositoryObservable);

        PinMutations pinMutations = new PinMutations(pinRepositoryObservable);
        TeamMutations teamMutations = new TeamMutations(teamRepositoryObservable);
        UserMutations userMutations = new UserMutations(userRepositoryObservable, pinRepositoryObservable, teamRepositoryObservable);
        
        RuntimeWiring runtimeWiring = RuntimeWiring.newRuntimeWiring()
                .type("Query", builder -> builder
                        .dataFetcher("pins", pinQueries::queryRoot)
                        .dataFetcher("teams", teamQueries::queryRoot)
                        .dataFetcher("users", userQueries::queryRoot)
                )
                .type("User", builder -> builder
                        .dataFetcher("pins", pinQueries.queryListWithFilter(UserQueryFilters::pins))
                        .dataFetcher("team", teamQueries.queryOneWithFilter(UserQueryFilters::team))
                )
                .type("Team", builder -> builder.dataFetcher("members", userQueries.queryListWithFilter(TeamQueryFilters::members)))
                .type("Pin", builder -> builder.dataFetcher("owners", userQueries.queryListWithFilter(PinQueryFilters::owners)))

                .type("Mutation", builder -> builder
                        .dataFetcher("createPin", pinMutations::create)
                        .dataFetcher("archivePin", pinMutations::archive)
                        .dataFetcher("createUser", userMutations::create)
                        .dataFetcher("removeUser", userMutations::remove)
                        .dataFetcher("givePin", userMutations::givePin)
                        .dataFetcher("joinTeam", userMutations::joinTeam)
                        .dataFetcher("createTeam", teamMutations::create)
                        .dataFetcher("removeTeam", teamMutations::remove)

                ).build();

        configRoutes(router -> { //TODO test if is possible to add a route after server start
            router.route("/graphql").handler(GraphQLHandler.create(getGraphQL(runtimeWiring)));
            router.route("/graphiql/*").handler(GraphiQLHandler.create(new GraphiQLHandlerOptions().setEnabled(true)));
        });
    }

    private GraphQL getGraphQL(RuntimeWiring runtimeWiring) {
        String schema = vertx.fileSystem().readFileBlocking("graphql.schema").toString();
        TypeDefinitionRegistry typeDefinitionRegistry = new SchemaParser().parse(schema);
        GraphQLSchema graphQLSchema = new SchemaGenerator().makeExecutableSchema(typeDefinitionRegistry, runtimeWiring);
        return GraphQL.newGraphQL(graphQLSchema).build();
    }
}
