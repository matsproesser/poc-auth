package poc.graphql.user;

import com.fasterxml.jackson.annotation.JsonProperty;
import poc.graphql.pin.Pin;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class User {

    @JsonProperty("_id")
    private String id;

    private String name;

    private String email;

    private String teamId;

    private Set<String> pins = new HashSet<>();

    private User() {

    }

    public User(String name, String email, String teamId) {
        this.name = name;
        this.email = email;
        this.teamId = teamId;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getTeamId() {
        return teamId;
    }

    public void setTeam(String teamId) {
        this.teamId = teamId;
    }

    public Set<String> getPins() {
        return pins;
    }

    public void addPin(String pinId) {
        this.pins.add(pinId);
    }

    public String getId() {
        return id;
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", team=" + teamId +
                ", pins=" + pins +
                '}';
    }
}
