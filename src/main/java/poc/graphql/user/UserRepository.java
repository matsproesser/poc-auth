package poc.graphql.user;

import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.Single;
import io.vertx.ext.mongo.MongoClient;
import poc.graphql.common.AbstractRepository;

public class UserRepository extends AbstractRepository<User> {

    private static final String USER_COLLECTION = "UserCollection";

    public UserRepository(Observable<MongoClient> mongoClientSource) {
        super(mongoClientSource, User.class, USER_COLLECTION);
    }

    public Single<Boolean> idExists(String id) {
        return findById(id).map(u -> true).defaultIfEmpty(false);
    }

}
