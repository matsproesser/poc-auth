package poc.graphql.user;

import graphql.schema.DataFetchingEnvironment;
import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.core.Maybe;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.Single;
import poc.graphql.common.BasicMutationResult;
import poc.graphql.pin.PinRepository;
import poc.graphql.pin.PinService;
import poc.graphql.team.TeamRepository;
import poc.graphql.team.TeamService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.CompletionStage;

public class UserMutations {

    private  PinService pinService;
    private TeamService teamService;
    public UserRepository repository;

    public UserMutations(Observable<UserRepository> userRepository, Observable<? extends PinService> pinService, Observable<? extends TeamService> teamService) {
        userRepository.subscribe(repository -> this.repository = repository);
        pinService.subscribe(service -> this.pinService = service);
        teamService.subscribe(service -> this.teamService = service);
    }

    public CompletionStage<BasicMutationResult> create(DataFetchingEnvironment environment) {
        User user = new User(environment.getArgument("name"), environment.getArgument("email"), environment.getArgument("teamId"));
        return repository.upsert(user).map(result -> new BasicMutationResult(result, 200, new ArrayList<>())).toCompletionStage();
    }

    public CompletionStage<BasicMutationResult> remove(DataFetchingEnvironment environment) {
        String id = environment.getArgument("id");
        return repository.remove(id).map(count -> new BasicMutationResult(id, 200)).toCompletionStage();
    }

    public CompletionStage<BasicMutationResult> givePin(DataFetchingEnvironment environment) {
        String pinId = environment.getArgument("pinId");
        String userId = environment.getArgument("userId");
        return Single.zip(pinService.idExists(pinId), repository.idExists(userId), (pinExists, userExists) -> pinExists && userExists)
                .flatMap(shouldUpdate -> {
                    if (!shouldUpdate) return Single.just(new BasicMutationResult("", 404, Arrays.asList("User or Pin not found")));
                    return repository.findById(userId).doOnSuccess(System.out::println)
                            .doOnSuccess(user -> user.addPin(pinId))
                            .flatMap(user -> repository.upsert(user)).defaultIfEmpty(userId)
                            .map(resultId -> new BasicMutationResult(resultId, 200));
                }).toCompletionStage();
    }

    public CompletionStage<BasicMutationResult> joinTeam(DataFetchingEnvironment environment) {
        String teamId = environment.getArgument("teamId");
        String userId = environment.getArgument("userId");
        return Single.zip(teamService.idExists(teamId), repository.idExists(userId), (teamExists, userExists) -> teamExists && userExists)
                .flatMap(shouldUpdate -> {
                    if (!shouldUpdate) return Single.just(new BasicMutationResult("", 404, Arrays.asList("User or Team not found")));
                    return repository.findById(userId)
                            .doOnSuccess(user -> user.setTeam(teamId)).doOnSuccess(System.out::println)
                            .flatMap(user -> repository.upsert(user)).defaultIfEmpty(userId)
                            .map(resultId -> new BasicMutationResult(resultId, 200)).doOnSuccess(System.out::println);
                }).toCompletionStage();
    }
}
