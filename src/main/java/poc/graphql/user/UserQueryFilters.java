package poc.graphql.user;

import graphql.schema.DataFetchingEnvironment;
import io.vertx.core.json.JsonObject;

import java.util.ArrayList;

public class UserQueryFilters {

    public static JsonObject pins(DataFetchingEnvironment environment) {
        User source = environment.getSource();
        return new JsonObject().put("_id", new JsonObject().put("$in", new ArrayList<>(source.getPins())));
    }
    public static JsonObject team(DataFetchingEnvironment environment) {
        User source = environment.getSource();
        return new JsonObject().put("_id", source.getTeamId());
    }
}
