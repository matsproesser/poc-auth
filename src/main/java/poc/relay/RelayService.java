package poc.relay;

import io.reactivex.rxjava3.core.Observable;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import io.vertx.rabbitmq.RabbitMQClient;

public class RelayService {

    private RabbitMQClient rabbitmq;

    public RelayService(Observable<RabbitMQClient> rabbitmq) {
        rabbitmq.subscribe(rabbit -> rabbit.start(handler -> {
            rabbit.exchangeDeclare("b2bExchange", "topic", true, false, h->{});
            rabbit.queueDeclare("b2bQueue", true, false, false, h->{});
            rabbit.queueBind("b2bQueue", "b2bExchange", "*", h->{});
            this.rabbitmq = rabbit;
        }));
    }

    public void upsertHandler(RoutingContext routingContext) {
        String country = routingContext.request().getHeader("country");
        String requestTraceId = routingContext.request().getHeader("requestTraceId");
        rabbitmq.basicPublish("b2bExchange", "*", new JsonObject().put("body", routingContext.getBodyAsJson().encode()), handler -> {
            if (handler.cause() != null)
                handler.cause().printStackTrace();
        });
        routingContext.response().setStatusCode(200).end("OK");
    }
}
