package poc.relay;

public class BaseMessage<T> {

    private String country;

    private String requestTraceId;

    private Long timestamp;

    private T data;

    public BaseMessage(String country, String requestTraceId, Long timestamp, T data) {
        this.country = country;
        this.requestTraceId = requestTraceId;
        this.timestamp = timestamp;
        this.data = data;
    }

    public String getCountry() {
        return country;
    }

    public String getRequestTraceId() {
        return requestTraceId;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public T getData() {
        return data;
    }
}