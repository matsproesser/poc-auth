package poc.relay;

import io.vertx.core.Vertx;
import io.vertx.rabbitmq.RabbitMQClient;
import poc.lib.cover.B2BAbstractVerticle;
import poc.lib.di.DependencyResolver;

public class RelayVerticle extends B2BAbstractVerticle {

    public static void main(String[] args) {
        Vertx.vertx().deployVerticle(new RelayVerticle());
    }
    @Override
    public void start(DependencyResolver dr) {
        RelayService relayService = new RelayService(dr.get(RabbitMQClient.class));
        configRoutes(router -> {
            router.post("/relay").handler(relayService::upsertHandler);
        });
    }

}
