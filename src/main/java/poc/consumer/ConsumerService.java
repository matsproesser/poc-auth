package poc.consumer;

import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.ObservableSource;
import io.vertx.core.AsyncResult;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.mongo.MongoClient;
import io.vertx.rabbitmq.RabbitMQConsumer;
import io.vertx.rabbitmq.RabbitMQMessage;
import poc.lib.reactive.ObservableHandler;
import poc.lib.reactive.SingleAsyncHandler;
import poc.lib.reactive.SingleHandler;

import static poc.lib.reactive.ObservableHelpers.asyncUnwrap;

public class ConsumerService {

    private MongoClient mongo;

    public ConsumerService(Observable<MongoClient> mongodb) {
        mongodb.subscribe(mongoInstance -> mongo = mongoInstance);
    }

    private static JsonObject convertToJsonData(String message) {
        return new JsonObject().put("queueValue", message);
    }

    private static String decodeMessage(RabbitMQMessage message) {
        return Json.decodeValue(message.body(), String.class);
    }

    private static ObservableSource<? extends RabbitMQMessage> handleQueue(RabbitMQConsumer rabbitMQConsumer) {
        return new ObservableHandler<RabbitMQMessage>().handling(rabbitMQConsumer::handler);
    }

    public ObservableHandler<AsyncResult<RabbitMQConsumer>> queueConsumer() {
        ObservableHandler<AsyncResult<RabbitMQConsumer>> obs = new ObservableHandler<>();
        Observable<? extends RabbitMQMessage> theRabbitQueue = obs.map(AsyncResult::result)
                .flatMap(ConsumerService::handleQueue);

        @NonNull Observable<JsonObject> messageStream = theRabbitQueue
                .map(ConsumerService::decodeMessage)
                .map(ConsumerService::convertToJsonData);

        messageStream.flatMap(this::saveMessage)
                .flatMap(asyncUnwrap())
                .doOnError(Throwable::printStackTrace)
                .subscribe();
        return obs;
    }

    private @NonNull Observable<AsyncResult<String>> saveMessage(JsonObject message) {
        return new SingleAsyncHandler<String>().handling(handle -> mongo.insert("consumerCollection", message, handle)).toObservable();
    }
}
