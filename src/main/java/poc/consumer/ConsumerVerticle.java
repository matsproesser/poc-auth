package poc.consumer;

import io.reactivex.rxjava3.core.Observable;
import io.vertx.ext.mongo.MongoClient;
import io.vertx.rabbitmq.RabbitMQClient;
import poc.lib.cover.B2BAbstractVerticle;
import poc.lib.cover.B2BLauncher;
import poc.lib.di.DependencyResolver;

public class ConsumerVerticle extends B2BAbstractVerticle {

    public static void main(String[] args) {
        B2BLauncher.launch(ConsumerVerticle::new);
    }

    @Override
    public void start(DependencyResolver dR) {
        ConsumerService consumerService = new ConsumerService(dR.get(MongoClient.class));

        Observable<RabbitMQClient> rabbitObservable = dR.get(RabbitMQClient.class);
        rabbitObservable.subscribe(rabbit -> rabbit.start(start -> {
            rabbit.exchangeDeclare("b2bExchange", "topic", true, false, h->{});
            rabbit.queueDeclare("b2bQueue", true, false, false, h->{});
            rabbit.queueBind("b2bQueue", "b2bExchange", "*", h->{});
            rabbit.basicConsumer("b2bQueue", consumerService.queueConsumer());
        }));
    }
}
