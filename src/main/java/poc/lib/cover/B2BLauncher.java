package poc.lib.cover;

import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.subjects.ReplaySubject;
import io.reactivex.rxjava3.subjects.Subject;
import io.vertx.config.ConfigRetriever;
import io.vertx.config.ConfigRetrieverOptions;
import io.vertx.config.ConfigStoreOptions;
import io.vertx.core.AsyncResult;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Vertx;
import io.vertx.core.VertxOptions;
import io.vertx.core.json.JsonObject;
import poc.lib.di.B2BDependencyManager;
import poc.lib.di.DependencyBindBuilder;
import poc.lib.reactive.ObservableHandler;
import poc.lib.reactive.SingleAsyncHandler;

import java.util.function.Supplier;

import static poc.lib.reactive.ObservableHelpers.asyncUnwrap;

public class B2BLauncher {

    private static Subject<B2BAbstractVerticle> launchables;
    private static void init() {
        launchables = ReplaySubject.create();
        Observable.just(new Context())
                .flatMap(B2BLauncher::getStartInstance)
                .flatMap(B2BLauncher::getConfigRetriever)
                .flatMap(B2BLauncher::updateConfigs)
                .flatMap(B2BLauncher::getDependencyManager)
                .flatMap(context -> launchables
                        .map(verticle -> {
                            DeploymentOptions deployOptions = new DeploymentOptions(context.config);
                            deployOptions.setConfig(context.config);
                            System.out.println("########################### verticle : "+verticle.toString());
                            context.vertx.deployVerticle(verticle, deployOptions);
                            context.configRetriever.listen(verticle::handleChange);
                            return verticle;
                        })
                ).subscribe(verticle -> System.out.println("subscribe verticle = " + verticle.toString()), Throwable::printStackTrace);
    }

    public static void launch(Supplier<B2BAbstractVerticle> supplier) {
        launch(supplier.get());
    }

    public static void launch(B2BAbstractVerticle verticle) {
        if (launchables == null) init();
        launchables.onNext(verticle);
    }

    private static Observable<Context> getStartInstance(Context context) {
        return Observable.create(source -> {
            if (context.vertx != null) {
                source.onNext(context);
            } else {
                Vertx vertx = Vertx.vertx();
                ConfigRetriever configRetriever = ConfigRetriever.create(vertx, getB2bConfigStores());
                configRetriever.getConfig(handler -> {
                    JsonObject configs = handler.result();
                    JsonObject vertxConfigs = configs.getJsonObject("vertx", new JsonObject());
                    VertxOptions options = new VertxOptions(vertxConfigs);
                    vertx.close();
                    configRetriever.close();
                    context.vertx = Vertx.vertx(options);
                    source.onNext(context);
                });
            }
        });
    }
    private static Observable<Context> getConfigRetriever(Context context) {
        if (context.configRetriever == null) {
            context.configRetriever = ConfigRetriever.create(context.vertx, getB2bConfigStores());
        }
        return Observable.just(context);
    }

    private static ConfigRetrieverOptions getB2bConfigStores() {
        ConfigStoreOptions dependencyManagerConfigStore = new ConfigStoreOptions()
                .setFormat("json").setType("file")
                .setConfig(new JsonObject().put("path", "conf/dependency-manager.json"));
        ConfigStoreOptions override = new ConfigStoreOptions()
                .setFormat("json").setType("file").setOptional(true)
                .setConfig(new JsonObject().put("path", "conf/override.json"));
        return new ConfigRetrieverOptions()
                .setIncludeDefaultStores(true)
                .addStore(dependencyManagerConfigStore)
                .addStore(override);
    }

    private static Observable<Context> getDependencyManager(Context context) {
        if (context.dependencyManager == null) {
            context.dependencyManager = B2BDependencyManager.instance();
            context.configRetriever.listen(configChange -> context.dependencyManager.configChange(configChange));
            return context.dependencyManager
                    .initConfig(context.config)
                    .doOnNext(b2BDependencyManager ->
                            b2BDependencyManager.bind(DependencyBindBuilder.fromType(Vertx.class).withCreator((a, b) -> context.vertx).neverUpdate().build()))
                    .map(o -> context);
        }
        return Observable.just(context);
    }

    private static Observable<Context> updateConfigs(Context context) {
        ObservableHandler<AsyncResult<JsonObject>> observableHandler = new ObservableHandler<>();
        context.configRetriever.getConfig(observableHandler);
        return observableHandler.flatMap(asyncUnwrap()).map(config -> {
            context.config = config;
            return context;
        });
    }

    private static class Context {
        private Vertx vertx;
        private ConfigRetriever configRetriever;
        private B2BDependencyManager dependencyManager;
        private JsonObject config;
    }
}
