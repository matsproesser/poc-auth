package poc.lib.cover;

import io.reactivex.rxjava3.core.Observable;
import io.vertx.config.ConfigChange;
import io.vertx.core.*;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;
import poc.lib.di.B2BDependencyManager;
import poc.lib.di.DependencyConfigure;
import poc.lib.di.DependencyResolver;

public abstract class B2BAbstractVerticle extends AbstractVerticle {

    private static final Logger logger = LoggerFactory.getLogger(B2BAbstractVerticle.class);
    private B2BDependencyManager dependencyManager;

    @Override
    public void init(Vertx vertx, Context context) {
        super.init(vertx, context);
        dependencyManager = B2BDependencyManager.instance();
    }

    @Override
    public final void start(Future<Void> startFuture) {
        Observable.just(dependencyManager)
                .doOnNext(this::configure)
                .flatMap(o -> Observable.fromCallable(() -> { start(o); return true; }))
                .doOnComplete(() -> logger.info("Verticle started with success " + this.getClass().getName()))
                .doOnError(err -> logger.warn("Failed to start verticle: " + this.getClass().getName(), err))
                .subscribe(o -> {}, startFuture::fail, startFuture::complete);
    }

    protected void configRoutes(Handler<Router> routerHandler) {
        Router router = Router.router(vertx);
        router.route().handler(BodyHandler.create());
        routerHandler.handle(router);
        router.route().failureHandler(this::errorHandler);
        //TODO: listen may fail async from verticle deploy so verticle may succeed falsely
        vertx.createHttpServer().requestHandler(router)
                .listen(8080, listen -> logger.info("Start on port [8080] succeed : " + listen.succeeded()));
    }

    protected final void handleChange(ConfigChange change) {
        context.config().mergeIn(change.getNewConfiguration(), true);
        configChanged(change);
        logger.info("change handled");
    }

    protected void configChanged(ConfigChange change) {

    }

    private void errorHandler(RoutingContext handler) {
        if (handler.failed()) {
            logger.warn("ERROR! ", handler.failure());
            handler.response().setStatusCode(500).end("error, see logs!!");
        }
    }

    public void configure(DependencyConfigure configurer) {}

    public abstract void start(DependencyResolver dependencyResolver);
}
