package poc.lib.di.vertxbindings;

import io.reactivex.rxjava3.core.Observable;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.mongo.MongoClient;
import poc.lib.di.DependencyBindConfiguration;
import poc.lib.di.DependencyResolver;

public class MongoDBBind implements DependencyBindConfiguration<MongoClient> {

    @Override
    public Class<MongoClient> dependencyType() {
        return MongoClient.class;
    }

    @Override
    public Observable<MongoClient> createDependency(JsonObject config, DependencyResolver dependencyResolver) {
        String dataSourceName = config.getJsonObject("mongodb").getString("connectionPool", "default");
        String host = config.getJsonObject("mongodb").getString("host");
        return dependencyResolver.get(Vertx.class)
                .map(vertx -> MongoClient.createShared(vertx, new JsonObject().put("host", host), dataSourceName));
    }
}
