package poc.lib.di.vertxbindings;

import io.reactivex.rxjava3.core.Observable;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.rabbitmq.RabbitMQClient;
import io.vertx.rabbitmq.RabbitMQOptions;
import poc.lib.di.DependencyBindConfiguration;
import poc.lib.di.DependencyResolver;

public class RabbitMQBind implements DependencyBindConfiguration<RabbitMQClient> {

    @Override
    public Class<RabbitMQClient> dependencyType() {
        return RabbitMQClient.class;
    }

    @Override
    public Observable<RabbitMQClient> createDependency(JsonObject config, DependencyResolver dependencyResolver) {
        RabbitMQOptions rabbitConfig = new RabbitMQOptions();
        rabbitConfig.setHost(config.getJsonObject("rabbitmq").getString("host")).setPassword("guest").setUser("guest").setPort(5672);
        return dependencyResolver.get(Vertx.class)
                .map(vertx -> RabbitMQClient.create(vertx, rabbitConfig))
                .flatMap(rabbit -> Observable.fromSingle(single -> rabbit.start(start -> single.onSuccess(rabbit))));
    }
}
