package poc.lib.di.vertxbindings;

import io.reactivex.rxjava3.core.Observable;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.redis.RedisClient;
import io.vertx.redis.RedisOptions;
import poc.lib.di.DependencyBindConfiguration;
import poc.lib.di.DependencyResolver;

public class RedisBind implements DependencyBindConfiguration<RedisClient> {

    @Override
    public Class<RedisClient> dependencyType() {
        return RedisClient.class;
    }

    @Override
    public Observable<RedisClient> createDependency(JsonObject config, DependencyResolver dependencyResolver) {
        return dependencyResolver.get(Vertx.class)
                .map(vertx -> RedisClient.create(vertx, new RedisOptions().setHost(config.getJsonObject("redis").getString("host"))));
    }
}
