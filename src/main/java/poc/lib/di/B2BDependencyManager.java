package poc.lib.di;

import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.functions.BiFunction;
import io.reactivex.rxjava3.subjects.BehaviorSubject;
import io.reactivex.rxjava3.subjects.Subject;
import io.vertx.config.ConfigChange;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import poc.lib.di.vertxbindings.MongoDBBind;
import poc.lib.di.vertxbindings.RabbitMQBind;
import poc.lib.di.vertxbindings.RedisBind;

import java.util.HashMap;
import java.util.Map;

import static io.reactivex.rxjava3.core.Observable.*;

public class B2BDependencyManager implements DependencyResolver, DependencyConfigure {

    public static final String LAST_VERSION = "lastVersion-B2BDependencyManager";
    private static B2BDependencyManager instance;
    private static Logger logger = LoggerFactory.getLogger(B2BDependencyManager.class);
    private JsonObject config;
    private boolean initialized = false;
    private Map<Class<?>, Subject<?>> dependencySources = new HashMap<>();
    private Subject<JsonObject> configEmitter = BehaviorSubject.create();

    private B2BDependencyManager() {
        configEmitter.subscribe(config -> this.config = config);
        bind(new RedisBind());
        bind(new RabbitMQBind());
        bind(new MongoDBBind());
    }

    @Override
    public void bind(DependencyBindConfiguration dependencyFactory) {
        // TODO triggar onNext quando nova dependencia for configurada(validar)
        Subject subject = dependencySources.getOrDefault(dependencyFactory.dependencyType(), BehaviorSubject.create());
        BiFunction<? super JsonObject, ? super B2BDependencyManager, Observable<?>> dependencyCombiner = (configs, dManager) -> {
            if (configs.getJsonObject(LAST_VERSION) == null || dependencyFactory.shouldUpdate(configs.getJsonObject(LAST_VERSION), configs)) {
                return dependencyFactory.createDependency(configs, dManager);
            }
            return empty();
        };
        combineLatest(configEmitter.scan(B2BDependencyManager::configScan), just(this), dependencyCombiner)
                .flatMap(o -> o)
                .subscribeWith(subject);
        dependencySources.putIfAbsent(dependencyFactory.dependencyType(), subject);
    }

    private static JsonObject configScan(JsonObject last, JsonObject nextConfig) {
        return new JsonObject().mergeIn(nextConfig).put(LAST_VERSION, last.putNull(LAST_VERSION));
    }

    public Observable<B2BDependencyManager> initConfig(JsonObject config) {
        if (!initialized) {
            this.initialized = true;
            configEmitter.onNext(config);
        }
        return just(this);
    }

    public void configChange(ConfigChange configChange) {
        this.config = configChange.getNewConfiguration();
        configEmitter.onNext(config);
    }

    @Override
    public <T> Observable<T> get(Class<T> dependency) {
        Subject<?> subject = dependencySources.getOrDefault(dependency, BehaviorSubject.create());
        dependencySources.putIfAbsent(dependency, subject);
        return (Observable<T>) subject.hide();
    }

    public static B2BDependencyManager instance() {
        if (instance == null) instance = new B2BDependencyManager();
        return instance;
    }
}
