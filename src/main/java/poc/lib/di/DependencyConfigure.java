package poc.lib.di;

public interface DependencyConfigure {
    void bind(DependencyBindConfiguration<?> dependencyFactory);
}
