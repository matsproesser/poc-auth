package poc.lib.di;

import io.reactivex.rxjava3.core.Observable;
import io.vertx.core.json.JsonObject;
import poc.lib.di.DependencyResolver;

public interface DependencyBindConfiguration<T> {

    Class<T> dependencyType();
    default boolean shouldUpdate(JsonObject oldConfig, JsonObject newConfig) {
        return true;
    }
    Observable<T> createDependency(JsonObject config, DependencyResolver dependencyResolver);
}
