package poc.lib.di;

import io.reactivex.rxjava3.core.Observable;

public interface DependencyResolver {
    <T> Observable<T> get(Class<T> dependency);
}
