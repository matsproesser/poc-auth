package poc.lib.di;

import io.reactivex.rxjava3.core.Observable;
import io.vertx.core.json.JsonObject;

import java.util.function.BiFunction;

public class DependencyBindBuilder<T> {

    private final Class<T> clazz;
    private BiFunction<JsonObject, JsonObject, Boolean> shouldUpdate = (a, b) -> true;
    private BiFunction<JsonObject, DependencyResolver, Observable<T>> creator = (a,b)-> Observable.error(new Exception("Creation method undefined"));
    public DependencyBindBuilder(Class<T> clazz) {
        this.clazz = clazz;
    }

    public static <T> DependencyBindBuilder<T> fromType(Class<T> clazz) {
        return new DependencyBindBuilder<T>(clazz);
    }

    public DependencyBindBuilder<T> withAsyncCreator(BiFunction<JsonObject, DependencyResolver, Observable<T>> creator) {
        this.creator = creator;
        return this;
    }
    public DependencyBindBuilder<T> withCreator(BiFunction<JsonObject, DependencyResolver, T> creator) {
        this.creator = (configs, resolver) -> Observable.just(creator.apply(configs, resolver));
        return this;
    }
    public DependencyBindBuilder<T> neverUpdate() {
        shouldUpdate = (a, b) -> false;
        return this;
    }
    public DependencyBindBuilder<T> withUpdateValidation(BiFunction<JsonObject, JsonObject, Boolean> shouldUpdate) {
        this.shouldUpdate = shouldUpdate;
        return this;
    }

    public DependencyBindWrapper<T> build() {
        return new DependencyBindWrapper<>(clazz, shouldUpdate, creator);
    }


    public void buildOn(DependencyConfigure configurer) {
        configurer.bind(this.build());
    }
}
