package poc.lib.di;

import io.reactivex.rxjava3.core.Observable;
import io.vertx.core.json.JsonObject;
import poc.lib.di.DependencyBindConfiguration;
import poc.lib.di.DependencyResolver;

import java.util.function.BiFunction;

public class DependencyBindWrapper<T> implements DependencyBindConfiguration<T> {

    private final Class<T> clazz;
    private final BiFunction<JsonObject, JsonObject, Boolean> shouldUpdate;
    private final BiFunction<JsonObject, DependencyResolver, Observable<T>> creator;

    public DependencyBindWrapper(Class<T> clazz, BiFunction<JsonObject, JsonObject, Boolean> shouldUpdate, BiFunction<JsonObject, DependencyResolver, Observable<T>> creator) {
        this.clazz = clazz;
        this.shouldUpdate = shouldUpdate;
        this.creator = creator;
    }

    @Override
    public Class<T> dependencyType() {
        return clazz;
    }

    @Override
    public boolean shouldUpdate(JsonObject oldConfig, JsonObject newConfig) {
        return shouldUpdate.apply(oldConfig, newConfig);
    }

    @Override
    public Observable<T> createDependency(JsonObject config, DependencyResolver dependencyResolver) {
        return creator.apply(config, dependencyResolver);
    }
}
