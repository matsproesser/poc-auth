package poc.lib.reactive;

import io.reactivex.rxjava3.core.Single;
import io.reactivex.rxjava3.core.SingleSource;
import io.reactivex.rxjava3.functions.Function;
import io.vertx.core.AsyncResult;

import java.util.Optional;

public class SingleHelpers {
    public static <T> Function<AsyncResult<T>, SingleSource<? extends T>> asyncUnwrap() {
        return result -> {
            if (result.failed()) return Single.error(result.cause());
            return Single.just(result.result());
        };
    }
    public static<T> Function<AsyncResult<T>, SingleSource<Optional<? extends T>>> asyncNullableUnwrap() {
        return result -> {
            if (result.failed()) return Single.error(result.cause());
            return Single.just(Optional.ofNullable(result.result()));
        };
    }
}
