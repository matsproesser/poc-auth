package poc.lib.reactive;

import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.Observer;
import io.vertx.core.Handler;

import java.util.ArrayList;
import java.util.List;

public class ObservableHandler<T> extends Observable<T> implements Handler<T> {

    protected T value = null;
    protected boolean complete = false;
    private List<Observer<? super T>> beforeCompleteObservers = new ArrayList<>();

    @Override
    protected void subscribeActual(Observer<? super T> observer) {
        if (complete) {
            respondObserver(observer);
        } else {
            beforeCompleteObservers.add(observer);
        }
    }

    @Override
    public void handle(T event) {
        value = event;
        complete = true;
        beforeCompleteObservers.forEach(this::respondObserver);
    }

    protected void respondObserver(Observer<? super T> observer) {
        if (value != null) observer.onNext(value);
    }

    public ObservableHandler<T> handling(Handler<Handler<T>> handler) {
        handler.handle(this);
        return this;
    }
}
