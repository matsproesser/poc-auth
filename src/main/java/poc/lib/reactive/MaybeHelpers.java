package poc.lib.reactive;

import io.reactivex.rxjava3.core.Maybe;
import io.reactivex.rxjava3.core.MaybeSource;
import io.reactivex.rxjava3.functions.Function;
import io.vertx.core.AsyncResult;

public class MaybeHelpers {
    public static <T> Function<AsyncResult<T>, MaybeSource<? extends T>> asyncUnwrap() {
        return result -> {
            if (result.failed()) return Maybe.error(result.cause());
            if (result.result() == null) return Maybe.empty();
            return Maybe.just(result.result());
        };
    }
}
