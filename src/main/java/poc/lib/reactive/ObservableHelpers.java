package poc.lib.reactive;

import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.ObservableSource;
import io.reactivex.rxjava3.functions.Function;
import io.vertx.core.AsyncResult;

public class ObservableHelpers {

    public static <T> Function<AsyncResult<T>, ObservableSource<? extends T>> asyncUnwrap() {
        return result -> {
            if (result.failed()) return Observable.error(result.cause());
            if (result.result() == null) return Observable.empty();
            return Observable.just(result.result());
        };
    }
}
