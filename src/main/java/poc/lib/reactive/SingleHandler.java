package poc.lib.reactive;

import io.reactivex.rxjava3.core.Single;
import io.reactivex.rxjava3.core.SingleObserver;
import io.vertx.core.Handler;

import java.util.ArrayList;
import java.util.List;

public class SingleHandler<T> extends Single<T> implements Handler<T> {

    protected T value = null;
    protected boolean complete = false;
    private List<SingleObserver<? super T>> beforeCompleteObservers = new ArrayList<>();

    @Override
    protected void subscribeActual(SingleObserver<? super T> observer) {
        if (complete) {
            respondObserver(observer);
        } else {
            beforeCompleteObservers.add(observer);
        }
    }

    @Override
    public void handle(T event) {
        value = event;
        complete = true;
        beforeCompleteObservers.forEach(this::respondObserver);
    }

    protected void respondObserver(SingleObserver<? super T> observer) {
        if (value != null) observer.onSuccess(value);
    }

    public SingleHandler<T> handling(Handler<Handler<T>> handler) {
        handler.handle(this);
        return this;
    }

}
