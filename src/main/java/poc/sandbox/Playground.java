package poc.sandbox;

import io.reactivex.rxjava3.core.Observable;

public class Playground {
    public static void main(String[] args) {

        Observable.just(1,2,3,4,5,6,7)
                .flatMap(n -> Observable.just(n*5).doOnComplete(() -> System.out.println("one done")))
                .subscribe(System.out::println,
                        Throwable::printStackTrace,
                        () -> System.out.println("COMPLETE")
                );
    }
}
