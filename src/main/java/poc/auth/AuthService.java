package poc.auth;

import io.reactivex.rxjava3.core.Observable;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.web.RoutingContext;
import io.vertx.redis.RedisClient;
import poc.auth.representations.ExchangeRequestRepresentation;

public class AuthService {

    private static final Logger logger = LoggerFactory.getLogger(AuthService.class);
    private RedisClient redisClient;
    private CredentialsHolder credentials;
    private JwtGenerator jwtGenerator;

    public AuthService(Observable<RedisClient> redisClient, Observable<CredentialsHolder> credentials, Observable<JwtGenerator> jwtGenerator) {
        redisClient.subscribe(redis -> this.redisClient = redis);
        credentials.subscribe(credentialsHolder -> this.credentials = credentialsHolder);
        jwtGenerator.subscribe(jwtGen -> this.jwtGenerator = jwtGen);
    }

    public void exchange(RoutingContext routingContext) {
        ExchangeRequestRepresentation body = routingContext.getBodyAsJson().mapTo(ExchangeRequestRepresentation.class);
        credentials.getCredentials().stream()
                .filter(cred -> cred.getAppId().equals(body.getAppId()) && cred.getAppSecret().equals(body.getAppSecret()))
                .findFirst()
                .ifPresentOrElse(
                cred -> routingContext.response().setStatusCode(201).end(new JsonObject().put("token", jwtGenerator.create(cred.getAppId(), cred.getRoles())).encodePrettily())
                , () -> routingContext.response().setStatusCode(401).end(new JsonObject().put("error", "invalid appId or appSecret").encodePrettily()));
    }

    public void saveToken(RoutingContext routingContext) {
        JsonObject body = routingContext.getBodyAsJson();
        String token = body.getString("token");
        String jwt = body.getString("jwt");
        redisClient.set(token, jwt, handler -> {
            if( handler.failed()) {
                handler.cause().printStackTrace();
            }
            routingContext.response()
                    .setStatusCode(200)
                    .end(defaultResponse(jwt));
        });
    }

    public void translate(RoutingContext routingContext) {
        String token = routingContext.request().getHeader("token");
        redisClient.get(token, handler -> routingContext.response().setStatusCode(201).end(defaultResponse(handler.result())));
    }

    private String defaultResponse(String jwt) {
        return new JsonObject().put("jwt", jwt).encodePrettily();
    }
}
