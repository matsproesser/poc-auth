package poc.auth;

import io.reactivex.rxjava3.core.Observable;
import io.vertx.core.json.JsonObject;
import poc.lib.di.DependencyBindConfiguration;
import poc.lib.di.DependencyResolver;

public class CredentialsHolderBind implements DependencyBindConfiguration<CredentialsHolder> {

    @Override
    public Class<CredentialsHolder> dependencyType() {
        return CredentialsHolder.class;
    }

    @Override
    public Observable<CredentialsHolder> createDependency(JsonObject config, DependencyResolver dependencyResolver) {
        return Observable.just(config.mapTo(CredentialsHolder.class));
    }
}
