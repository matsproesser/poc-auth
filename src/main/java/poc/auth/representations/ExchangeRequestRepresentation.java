package poc.auth.representations;

public class ExchangeRequestRepresentation {

    private String appId;

    private String appSecret;

    public String getAppId() {
        return appId;
    }

    public String getAppSecret() {
        return appSecret;
    }

}
