package poc.auth;

import io.reactivex.rxjava3.core.Observable;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import poc.lib.di.DependencyBindConfiguration;
import poc.lib.di.DependencyResolver;

public class JwtGeneratorBind implements DependencyBindConfiguration<JwtGenerator> {

    @Override
    public Class<JwtGenerator> dependencyType() {
        return JwtGenerator.class;
    }

    @Override
    public Observable<JwtGenerator> createDependency(JsonObject config, DependencyResolver dependencyResolver) {
        return dependencyResolver.get(Vertx.class)
                .map(vertx -> new JwtGenerator(vertx, config.getString("jwtSecret"), config.getLong("expirationTime")));
    }
}
