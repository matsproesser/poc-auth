package poc.auth;

import io.vertx.core.Vertx;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.auth.PubSecKeyOptions;
import io.vertx.ext.auth.jwt.JWTAuth;
import io.vertx.ext.auth.jwt.JWTAuthOptions;

import java.time.Instant;
import java.util.Date;
import java.util.List;

public class JwtGenerator {

    private static final String AUTH_SERVICE = "auth-service";
    private final JWTAuth provider;
    private String key;
    private final Long expirationTime;

    public JwtGenerator(Vertx vertx, String secret, Long expirationTime) {
        this.key = secret;
        this.expirationTime = expirationTime;
        this.provider = JWTAuth.create(vertx, new JWTAuthOptions()
                .addPubSecKey(new PubSecKeyOptions()
                        .setAlgorithm("HS256")
                        .setPublicKey(key)
                        .setSymmetric(true)));
    }

    public String create(String appId, List<String> roles) {
        final long currentTimeInMillis = Instant.now().toEpochMilli();
        try {
            JsonArray rolesArray = new JsonArray();
            roles.forEach(rolesArray::add);
            return provider.generateToken(new JsonObject()
                    .put("app", appId)
                    .put("issuer", AUTH_SERVICE)
                    .put("subject", "ab-inbev")
                    .put("audience", AUTH_SERVICE)
                    .put("issuedAt", new Date(currentTimeInMillis).getTime())
                    .put("expiresAt", new Date(currentTimeInMillis + expirationTime).getTime())
                    .put("roles", rolesArray)
            );
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
