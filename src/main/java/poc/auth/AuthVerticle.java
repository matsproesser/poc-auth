package poc.auth;

import io.vertx.core.DeploymentOptions;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.redis.RedisClient;
import poc.lib.cover.B2BAbstractVerticle;
import poc.lib.di.DependencyConfigure;
import poc.lib.di.DependencyResolver;

public class AuthVerticle extends B2BAbstractVerticle {

    private static final Logger logger = LoggerFactory.getLogger(AuthVerticle.class);

    public static void main(String[] args) {
        Vertx.vertx().deployVerticle(AuthVerticle.class, new DeploymentOptions());
    }

    @Override
    public void configure(DependencyConfigure configure) {
        configure.bind(new JwtGeneratorBind());
        configure.bind(new CredentialsHolderBind());
    }

    @Override
    public void start(DependencyResolver dR) {
        AuthService authService = new AuthService(dR.get(RedisClient.class), dR.get(CredentialsHolder.class), dR.get(JwtGenerator.class));
        configRoutes(router -> {
            router.route(HttpMethod.POST, "/exchange").handler(authService::exchange);
            router.route(HttpMethod.POST, "/save-token").handler(authService::saveToken);
            router.route(HttpMethod.POST, "/translate").handler(authService::translate);
        });
    }
}
