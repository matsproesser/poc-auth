package poc.auth;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CredentialsHolder {
    private List<Credential> credentials;

    public List<Credential> getCredentials() {
        return credentials;
    }

    public static class Credential {
        private String appId;
        private String appSecret;
        private List<String> roles;

        public String getAppId() {
            return appId;
        }
        public String getAppSecret() {
            return appSecret;
        }
        public List<String> getRoles() {
            return roles;
        }
    }

}

