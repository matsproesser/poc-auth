package poc.verticles.example;

import io.vertx.core.AbstractVerticle;

import java.util.concurrent.atomic.AtomicInteger;

public class MessageReceiverVerticle extends AbstractVerticle {

    final AtomicInteger count = new AtomicInteger();
    @Override
    public void start() throws Exception {
        vertx.eventBus().consumer("common-address", handler -> {
            System.out.println("Message received: " + handler.body());
        });
    }
}
