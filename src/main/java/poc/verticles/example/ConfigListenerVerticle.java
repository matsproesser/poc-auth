package poc.verticles.example;

import io.vertx.config.ConfigRetriever;
import io.vertx.config.ConfigRetrieverOptions;
import io.vertx.config.ConfigStoreOptions;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;

public class ConfigListenerVerticle extends AbstractVerticle {

    public static void main(String[] args) {
        Vertx.vertx().deployVerticle(new ConfigListenerVerticle());
    }

    @Override
    public void start() throws Exception {
        ConfigRetrieverOptions options = new ConfigRetrieverOptions();
        options.addStore(new ConfigStoreOptions()
                .setFormat("json")
                .setOptional(false)
                .setType("file")
                .setConfig(
                        new JsonObject().put("path", "conf/config.json")
                )
        );
        ConfigRetriever configRetriever = ConfigRetriever.create(vertx, options);
        configRetriever.getConfig(handler -> {
            System.out.println("FIRST CONFIG " + handler.result().encodePrettily());
        });
        configRetriever.listen(change -> {
            System.out.println("NEW CHANGE= " + change.getNewConfiguration().encodePrettily());
        });
    }
}
