package poc.verticles.example;

import io.vertx.core.AbstractVerticle;

public class HelloVerticle extends AbstractVerticle {

    @Override
    public void start() {
        System.out.println("Welcome to Vertx");
    }

    @Override
    public void stop() throws Exception {
        System.out.println("shutting down verticle");
    }
}