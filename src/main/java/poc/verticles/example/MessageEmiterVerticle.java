package poc.verticles.example;

import io.vertx.core.AbstractVerticle;

import java.util.concurrent.atomic.AtomicInteger;

public class MessageEmiterVerticle extends AbstractVerticle {

    final AtomicInteger count = new AtomicInteger();
    @Override
    public void start() throws Exception {
        vertx.setPeriodic(2000,
                handler ->  {
                    System.out.println("sending message");
                    vertx.eventBus().publish("common-address", "message count :" + count.addAndGet(1));
                    System.out.println("message sent");
                }
        );


    }
}
