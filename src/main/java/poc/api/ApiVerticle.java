package poc.api;

import io.vertx.ext.mongo.MongoClient;
import poc.consumer.ConsumerVerticle;
import poc.lib.cover.B2BAbstractVerticle;
import poc.lib.cover.B2BLauncher;
import poc.lib.di.DependencyResolver;

public class ApiVerticle extends B2BAbstractVerticle {

    public static void main(String[] args) {
        B2BLauncher.launch(ConsumerVerticle::new);
        B2BLauncher.launch(ApiVerticle::new);
    }
    @Override
    public void start(DependencyResolver dependencyResolver) {
        ApiService apiService = new ApiService(dependencyResolver.get(MongoClient.class));
        configRoutes(routes -> routes.get("/api").handler(apiService::listCollection));
    }
}
