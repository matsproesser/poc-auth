package poc.api;

import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.functions.Consumer;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.mongo.MongoClient;
import io.vertx.ext.web.RoutingContext;
import poc.lib.reactive.SingleAsyncHandler;

import java.util.List;

import static poc.lib.reactive.SingleHelpers.asyncUnwrap;

public class ApiService {

    private MongoClient mongo;

    public ApiService(Observable<MongoClient> mongo) {
        mongo.subscribe(mongoClient -> this.mongo = mongoClient);
    }

    public void listCollection(RoutingContext routingContext) {
        SingleAsyncHandler<List<JsonObject>> observableHandler = new SingleAsyncHandler<>();
        observableHandler.flatMap(asyncUnwrap())
                .doOnError(Throwable::printStackTrace)
                .doOnSuccess(next -> System.out.println(next.toString()))
                .subscribe(getListConsumer(routingContext));

        mongo.find("consumerCollection", new JsonObject(), observableHandler);
    }

    private Consumer<List<JsonObject>> getListConsumer(RoutingContext routingContext) {
        return next -> {
            JsonArray responseList = new JsonArray();
            next.forEach(responseList::add);
            routingContext.response().end(responseList.encodePrettily());
        };
    }
}
