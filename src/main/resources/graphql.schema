type Query {
  pins: [Pin]
  teams(qtde: Int = 1): [Team]
  users(teamId: String): [User]
}

type Mutation {
    createPin(name: String!, description: String!): BasicMutationResponse
    archivePin(id: String!): BasicMutationResponse

    createUser(name: String!, email: String!, teamId: String): BasicMutationResponse
    removeUser(id: String!): BasicMutationResponse
    givePin(pinId: String!, userId: String!): BasicMutationResponse
    joinTeam(teamId: String!, userId: String!): BasicMutationResponse

    createTeam(name: String!, mainChannel: String!, members: [String]): BasicMutationResponse
    removeTeam(id: String!): BasicMutationResponse
}

type BasicMutationResponse {
    id: String
    status: Int
    errors: [String]
}

type User {
    id: String!
    name: String!
    email: String!
    team: Team
    pins: [Pin]
}

type Team {
    id: String!
    name: String!
    mainChannel: String!
    members: [User]
}

type Pin {
    id: String
    name: String!
    description: String!
    owners: [User]
}